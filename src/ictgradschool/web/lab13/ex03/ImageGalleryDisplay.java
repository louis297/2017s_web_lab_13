package ictgradschool.web.lab13.ex03;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;

public class ImageGalleryDisplay extends HttpServlet {
    public ImageGalleryDisplay() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try(PrintWriter out = response.getWriter()){
            out.println("<html>");
            out.println("<head>");
            out.println("<title>ex03</title>");
            out.println("</head>");
            out.println("<body>");

            ServletContext servletContext = getServletContext();
            String fullPhotoPath = servletContext.getRealPath("/Photos");
            File dir = new File(fullPhotoPath);

            File[] matchingFiles = dir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String fileName) {
                    return fileName.endsWith("_thumbnail.png");
                }
            }
            );
            for (File file : matchingFiles) {
                File fi = new File(file.getPath().replace("_thumbnail.png", ".jpg"));
                out.println("<div style=\"width:200px;\"><fieldset>");
                out.println("<legend>" + file.getName().substring(0, file.getName().lastIndexOf("_")) + "</legend>");
                out.println("<a href=\"/Photos/" + fi.getName() + "\">");
                out.println("<img src=\""+ "/Photos/" + file.getName() +"\" /></a>");

                out.println("<br/>Size:" + fi.length());
                out.println("</fieldset></div>");

            }
            out.println("</body>");
            out.println("</html>");
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
